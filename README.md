Reproduce
=========

1. Open shell with all dependencies (ninja, inkscape, pdftk...): `nix-shell -A shell`
2. Generate the figures: `ninja`
