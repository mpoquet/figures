#import "@preview/cetz:0.2.2"

#let pagescale = 0.95
#set page(width: 16cm * pagescale, height: 9cm * pagescale, margin: 5mm)
#set raw(theme: "mpoquet.tmTheme")

#show math.equation: block.with(fill: white, inset: 1pt)
#let il = -4mm

#cetz.canvas(length: 3cm, {
  import cetz.draw: *

  let theox = 0mm
  let implemx = 4.5cm
  let mutexy = 0cm
  let semy = -5cm

  let y = mutexy
  let x = implemx
  let x-border = 2mm
  let rect-width = 9.25cm
  let rect-height = 2.75cm
  rect((x - x-border, y), (x + rect-width + x-border, y - rect-height), stroke: .1mm, fill: none)
  content((x + rect-width / 2, y), anchor: "center", [
    #box(fill: luma(230), inset: (x: 2mm, y: 1.25mm), stroke: .2mm, [`mutex` API (C, POSIX threads)])
  ])
  let y = y + il/4
  content((x, y + il*1), anchor: "west", raw("#include <pthread.h>", lang: "c"))
  content((x, y + il*2), anchor: "west", raw("int pthread_mutex_init(...);", lang: "c"), name: "pthread-mutex-init")
  content((x, y + il*3), anchor: "west", raw("pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;", lang: "c"), name: "pthread-mutex-init-macro")
  content((x, y + il*4), anchor: "west", raw("int pthread_mutex_destroy(...);", lang: "c"))
  content((x, y + il*5), anchor: "west", raw("int pthread_mutex_lock(...);", lang: "c"), name: "pthread-mutex-lock")
  content((x, y + il*6), anchor: "west", raw("int pthread_mutex_unlock(...);", lang: "c"), name: "pthread-mutex-unlock")

  let x = theox
  let y = mutexy
  //content((x, y), anchor: "west", [Sémaphore théorique])
  let y = y + il/4
  content((x, y + il*2), anchor: "west", [`init(1)`], name: "theo-init")
  content((x, y + il*5), anchor: "west", [`P()`], name: "theo-p")
  content((x, y + il*6), anchor: "west", [`V()`], name: "theo-v")

  line("theo-init.east", (4.475cm, y + il*2), mark: none, padding: 1cm, stroke: .3mm)
  line((3cm, y + il*2), (3cm, y + il*3), (4.475cm, y + il*3), mark: none, padding: 1cm, stroke: .3mm)
  line("theo-p.east", "pthread-mutex-lock.west", mark: none, padding: 1cm, stroke: .3mm)
  line("theo-v.east", "pthread-mutex-unlock.west", mark: none, padding: 1cm, stroke: .3mm)

  let x = implemx
  let y = semy
  let rect-width = 9.25cm
  let rect-height = 2.4cm
  rect((x - x-border, y), (x + rect-width + x-border, y - rect-height), stroke: .1mm, fill: none)
  content((x + rect-width / 2, y), anchor: "center", [
    #box(fill: luma(230), inset: (x: 2mm, y: 1.25mm), stroke: .2mm, [`sem` API (C, POSIX threads)])
  ])
  let y = y + il/4
  content((x, y + il*1), anchor: "west", raw("#include <semaphore.h>", lang: "c"))
  content((x, y + il*2), anchor: "west", raw("int sem_init(..., unsigned int n);", lang: "c"), name: "pthread-sem-init")
  content((x, y + il*3), anchor: "west", raw("int sem_destroy(...);", lang: "c"))
  content((x, y + il*4), anchor: "west", raw("int sem_wait(...);", lang: "c"), name: "pthread-sem-wait")
  content((x, y + il*5), anchor: "west", raw("int sem_post(...);", lang: "c"), name: "pthread-sem-post")

  let x = theox
  let y = semy
  //content((x, y), anchor: "west", [Sémaphore théorique])
  let y = y + il/4
  content((x, y + il*2), anchor: "west", [`init(n)`], name: "theo-init")
  content((x, y + il*4), anchor: "west", [`P()`], name: "theo-p")
  content((x, y + il*5), anchor: "west", [`V()`], name: "theo-v")

  line("theo-init.east", (4.475cm, y + il*2), mark: none, padding: 1cm, stroke: .3mm)
  line("theo-p.east", "pthread-sem-wait.west", mark: none, padding: 1cm, stroke: .3mm)
  line("theo-v.east", "pthread-sem-post.west", mark: none, padding: 1cm, stroke: .3mm)
})
