import fontsize;
usepackage("amsfonts");

settings.outformat = "pdf";
int frame_id = -1;

defaultpen(font("OT1","cmr","m","n") + fontsize(30pt));
unitsize(1cm);

real scale = 10;
real eps_scale = 2.5;
real width = 4 * scale;
real height = 3 * scale;
pen magenta = rgb("A626A4");

string eps_memory_circuit_figure = graphic("./memory-circuit-figure.eps");
string eps_values_binary = graphic("./64bit-values-binary.eps");
string eps_values_decimal = graphic("./64bit-values-decimal.eps");
string eps_values_hexa = graphic("./64bit-values-hexa.eps");
string eps_addr_values_hexa = graphic("./64bit-addr-values-hexa.eps");
string eps_addr_read = graphic("./64bit-addr-read.eps");
string eps_addr_pre_write = graphic("./64bit-addr-pre-write.eps");
string eps_addr_write = graphic("./64bit-addr-write.eps");
string eps_addr_values_hexa_final = graphic("./64bit-addr-values-hexa-final.eps");

pair eps_pos = (0.97*width, height*0.05);
pair eps_fix_offset = (0.0015*width,0);
pair base_text_pos = (0.03*width, height*0.85);
pair definition_base_pos = (0.03*width, height*0.6);
pair operation_base_pos = (0.03*width, height*0.3);
pair newline_offset = (0, -height*0.05);

void draw_background(bool with_border)
{
    path background = (0, 0) -- (width, 0) -- (width, height) -- (0, height) -- cycle;
    fill(background, white);
    if (with_border)
        draw(background, black);
}

draw_background(true);
label("\textbf{La mémoire}", (width/2, height*0.95));
shipout(format("memory-%02d", ++frame_id));

label("C'est un composant essentiel qui sert à stocker des données.", base_text_pos, align=SE);
save();
label(scale(eps_scale)*Label(eps_memory_circuit_figure, eps_pos, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("Défini comme une suite finie de \textbf{cases} mémoires.", definition_base_pos, align=SE);
label("- L'exemple (à droite) comporte 8 cases", definition_base_pos + (0.01*width, 0) + newline_offset*1, align=SE);
save();
label("- Chaque case contient \textbf{1 octet} : 8 bits", definition_base_pos + (0.01*width, 0) + newline_offset*2, align=SE);
label(scale(eps_scale)*Label(eps_values_binary, eps_pos, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
save();
label("- Chaque case contient \textbf{1 octet} : un entier $0 \leq n \leq 255$", definition_base_pos + (0.01*width, 0) + newline_offset*2, align=SE);
label(scale(eps_scale)*Label(eps_values_decimal, eps_pos, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("- Chaque case contient \textbf{1 octet} : 2 chiffres héxadécimaux", definition_base_pos + (0.01*width, 0) + newline_offset*2, align=SE);
save();
label(scale(eps_scale)*Label(eps_values_hexa, eps_pos, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("- Chaque case est identifiée par son \textbf{adresse}", definition_base_pos + (0.01*width, 0) + newline_offset*3, align=SE);
save();
label(scale(eps_scale)*Label(eps_addr_values_hexa, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("Deux opérations sont possibles sur la mémoire.", operation_base_pos, align=SE);
save();
label(scale(eps_scale)*Label(eps_addr_values_hexa, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("- \textbf{Lire} la valeur d'une case : \texttt{read(0x02)} $\rightarrow$ \texttt{0x2C}", operation_base_pos + (0.01*width, 0) + newline_offset*1, align=SE);
save();
label(scale(eps_scale)*Label(eps_addr_read, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
save();
label(scale(eps_scale)*Label(eps_addr_values_hexa, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label("- \textbf{Écrire} la valeur d'une case : \texttt{write(0x04, 0x6D)}", operation_base_pos + (0.01*width, 0) + newline_offset*1.8, align=SE);
save();
label(scale(eps_scale)*Label(eps_addr_pre_write, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
save();
label("~~(l'ancienne valeur de la case est perdue)", operation_base_pos + (0.01*width, 0) + newline_offset*3, align=SE);
label(scale(eps_scale)*Label(eps_addr_write, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));

restore();
label(scale(eps_scale)*Label(eps_addr_values_hexa_final, eps_pos + eps_fix_offset, align=NW));
shipout(format("memory-%02d", ++frame_id));
