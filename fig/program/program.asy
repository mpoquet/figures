import fontsize;

settings.outformat = "pdf";
int frame_id = -1;

defaultpen(font("OT1","cmr","m","n") + fontsize(30pt));
unitsize(1cm);

real scale = 10;
real width = 4 * scale;
real height = 3 * scale;
pen magenta = rgb("A626A4");

string src_text_black = graphic("./src-text-black.eps");
string src_text_color = graphic("./src-text-color.eps");
string src_bg_color = graphic("./src-bg-color.eps");
string bin_singleline_base2_text = graphic("./bin-singleline-base2-text.eps");
string bin_singleline_text = graphic("./bin-singleline-text.eps");
string bin_multiline_text = graphic("./bin-multiline-text.eps");
string bin_multiline_bg_color  = graphic("./bin-multiline-bg-color.eps");
string asm_text_black = graphic("./asm-text-black.eps");
string asm_bg_color = graphic("./asm-bg-color.eps");

pair base_text_dev = (width*0.53, height*0.85);
pair newline_offset = (0, -height*0.05);
pair newline_offset2 = (0, -height*0.04);

void draw_background(bool with_border)
{
    path background = (0, 0) -- (width, 0) -- (width, height) -- (0, height) -- cycle;
    fill(background, white);
    if (with_border)
        draw(background, black);
}

void draw_code(bool colored_font, bool with_background)
{
    pair code_pos = (width*0.03, height*0.9);

    if (with_background)
        label(src_bg_color, code_pos, align=SE);

    if (colored_font)
        label(src_text_color, code_pos, align=SE);
    else
        label(src_text_black, code_pos, align=SE);
}

void draw_vision_dev(bool with_details)
{
    label("\textbf{Vision conception/développement}", base_text_dev, align=SE);

    if (with_details) {
        label(" - code source", base_text_dev + 1*newline_offset, align=SE);
        label(" - algorithmes", base_text_dev + 2*newline_offset, align=SE);
        label(" - composants logiciels", base_text_dev + 3*newline_offset, align=SE);
    }
}

void draw_vision_sysarch()
{
    label("\textbf{Vision système/architecture}", base_text_dev - (0,0.78*height), align=SE);
}

void draw_mid_separator()
{
    real separator_height = 0.575;
    real separator_margin = 0.02;
    draw((width*separator_margin, separator_height*height) -- (width*(1-separator_margin), separator_height*height), linewidth(2));
}

void draw_bin_multiline(bool with_background)
{
    pair pos = (width*0.03, height*0.20);
    if (with_background)
        draw(bin_multiline_bg_color, pos, align=SE);
    draw(bin_multiline_text, pos, align=SE);
}

void draw_asm(bool with_background)
{
    pair pos = (width*0.6, height*0.85);
    if (with_background)
        draw(asm_bg_color, pos, align=SE);
    draw(asm_text_black, pos, align=SE);
}

draw_background(true);
label("\textbf{Qu'est-ce qu'un programme ?}", (width/2, height*0.95));
shipout(format("program-%02d", ++frame_id));
save();

picture pic = currentpicture;

draw_code(true, false);
draw_vision_dev(true);
// TODO: schéma tableau/index/exemple, algorigramme
shipout(format("program-%02d", ++frame_id));

draw_mid_separator();
draw_vision_sysarch();
shipout(format("program-%02d", ++frame_id));

pair base_text_problem = (width*0.03, height*0.55);
label("\textbf{Problème : un processeur ne comprend aucun de ces langages !}", base_text_problem, align=SE, rgb("e00000"));
shipout(format("program-%02d", ++frame_id));

save();
label("\textbf{Jetons un œil au fonctionnement d'un ordinateur pour éclaircir ça...}", base_text_problem + newline_offset*2, align=SE);
label("\textbf{- La mémoire}", base_text_problem + newline_offset*3, align=SE);
label("\textbf{- Le processeur}", base_text_problem + newline_offset*4, align=SE);
shipout("program-ouverture-archi");
label("\textbf{OK ! On peut revenir sur ce qu'est un programme :)}", base_text_problem + newline_offset*6, align=SE);
shipout("program-fermeture-archi");
restore();

label("Un processeur ne parle que son \textbf{langage machine} : des intructions en binaire !", base_text_problem + newline_offset*1, align=SE);

draw(bin_singleline_base2_text, (width*0.03, height*0.4), align=SE);
draw("la fonction \textit{index\_of} est une suite de 74 octets", (width*0.45, height*0.41), align=SE);
shipout(format("program-%02d", ++frame_id));

draw(bin_singleline_text, (width*0.03, height*0.35), align=SE);
draw("... que l'on peut représenter en base 16", (width*0.45, height*0.36), align=SE);
draw("(2 caractères = 1 octet)", (width*0.48, height*0.31), align=SE);
shipout(format("program-%02d", ++frame_id));

draw_bin_multiline(false);
draw("... que l'on peut représenter par bloc", (width*0.45, height*0.21), align=SE);
draw("(1 ligne = 16 octets)", (width*0.48, height*0.16), align=SE);
shipout(format("program-%02d", ++frame_id));

restore();
draw("\textbf{Ce code machine = ce code C ?}", (width*0.03, height*0.55), align=SE, rgb("00a000"));
save();
draw_code(true, false);
draw_bin_multiline(false);
shipout(format("program-%02d", ++frame_id));

restore();
draw_code(false, true);
draw_bin_multiline(true);
pair oui_base_text = (width*0.03, height*0.48);
draw("\textbf{Oui !} Chaque \textit{bout} du code machine", oui_base_text, align=SE);
draw("vient d'instructions/données du code C", oui_base_text + newline_offset2*1, align=SE);
shipout(format("program-%02d", ++frame_id));

pair asm_base_text = (width*0.03, height*0.35);
draw("- Ce lien est plus clair en \textbf{assembleur}", oui_base_text - (0,0.2) + newline_offset2*2, align=SE);
draw("~~(code machine $\simeq$ assembleur)", oui_base_text - (0,0.2) + newline_offset2*3, align=SE);
// TODO: montrer instructions (cmp) / données (-1)
//draw("- Explications/détails pas dans ce cours", oui_base_text - (0,0.4) + newline_offset2*4, align=SE);
//draw("~~(ce sera en cours d'archi et de compilation)", oui_base_text - (0,0.4) + newline_offset2*5, align=SE);
draw_asm(true);
shipout(format("program-%02d", ++frame_id));
