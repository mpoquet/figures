{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.05.tar.gz";
    sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
  }) {}
}:

let
  self = rec {
    shell = pkgs.mkShell {
      buildInputs = with pkgs; [
        ninja
        inkscape
        pdftk
      ];
    };
    figures = pkgs.stdenv.mkDerivation {
      name = "figures";
      buildInputs = shell.buildInputs;
      src = pkgs.lib.sourceByRegex ./. [
        "build\.ninja"
        "fig"
        "fig/.*\.svg"
      ];
      installPhase = ''
        mkdir -p $out
        cp pipe-usage-example.pdf $out/
      '';
    };
  };
in
  self
