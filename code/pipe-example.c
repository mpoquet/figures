#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
  char c; int p[2];
  pipe(p);
  switch(fork()) {
    case 0:
      close(p[0]);
      dup2(p[1], 1);
      execlp("echo", "echo", "hello!", NULL);
      exit(1);
    default:
      close(p[1]);
      while (read(p[0], &c, sizeof(char)) > 0)
        write(1, &c, sizeof(char));
      close(p[0]);
      exit(0);
  }
}
