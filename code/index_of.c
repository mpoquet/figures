// Renvoie l'indice de la première occurrence de value dans le tableau array.
// Renvoie -1 si value n'est pas dans array.
int index_of(int value, int size, int * array)
{
  for (int i = 0; i < size; ++i) {
    if (array[i] == value)
      return i;
  }
  return -1;
}

// Test de la fonction.
#include <stdio.h>
#include <stdlib.h>
#define CHECK(ARRAY, SIZE, VALUE, POS) \
  if (index_of(VALUE, SIZE, ARRAY) != POS) {\
    fprintf(stderr, "index_of(%d) should be %d\n", VALUE, POS);\
    exit(1);\
  }

int main() {
  int array[6] = {1, 2, 3, 4, 2, 6};
  CHECK(array, 6, 0, -1);
  CHECK(array, 6, 42000, -1);
  CHECK(array, 6, 1, 0);
  CHECK(array, 6, 2, 1);
  CHECK(array, 6, 3, 2);
  CHECK(array, 6, 4, 3);
  CHECK(array, 6, 6, 5);

  CHECK(NULL, 0, 0, -1);
  CHECK(NULL, 0, 42000, -1);
  return 0;
}
